-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Hostiteľ: 127.0.0.1
-- Čas generovania: Št 08.Dec 2016, 21:58
-- Verzia serveru: 5.7.14
-- Verzia PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáza: `euba`
--

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `akcie_leto`
--

CREATE TABLE `akcie_leto` (
  `datum` varchar(10) NOT NULL,
  `oblast` varchar(20) NOT NULL,
  `mesto` varchar(20) NOT NULL,
  `cena` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `akcie_leto`
--

INSERT INTO `akcie_leto` (`datum`, `oblast`, `mesto`, `cena`) VALUES
('19.2.2016', 'Kreta', 'malia', 200),
('19.3.2016', 'Kreta', 'chania', 250);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `akcie_poznavacie`
--

CREATE TABLE `akcie_poznavacie` (
  `datum` varchar(30) NOT NULL,
  `oblast` varchar(30) NOT NULL,
  `mesto` varchar(30) NOT NULL,
  `cena` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `akcie_poznavacie`
--

INSERT INTO `akcie_poznavacie` (`datum`, `oblast`, `mesto`, `cena`) VALUES
('20.1.2017', 'Vysoke_tatry', 'Strbske_pleso', 100);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `kontakt`
--

CREATE TABLE `kontakt` (
  `mesto` varchar(30) NOT NULL,
  `adresa` varchar(30) NOT NULL,
  `psc` varchar(30) NOT NULL,
  `mail` varchar(30) NOT NULL,
  `cislo` varchar(30) NOT NULL,
  `poznamka` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `kontakt`
--

INSERT INTO `kontakt` (`mesto`, `adresa`, `psc`, `mail`, `cislo`, `poznamka`) VALUES
('Bratislava', 'Dolnozemska cesta c.1 ', '911 01', 'ckeuba@euba.sk', '0903 345 678', '');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `letne`
--

CREATE TABLE `letne` (
  `destinacia` varchar(30) NOT NULL,
  `subor` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `letne`
--

INSERT INTO `letne` (`destinacia`, `subor`) VALUES
('Kreta', 'kreta.php'),
('Severne_taliansko', 'taliansko.php');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `objednavky`
--

CREATE TABLE `objednavky` (
  `ID` int(11) NOT NULL,
  `destinacia` varchar(30) NOT NULL,
  `letovisko` varchar(30) NOT NULL,
  `cena_osoba` varchar(11) NOT NULL,
  `pocet_miest` varchar(11) NOT NULL,
  `cena_celkova` varchar(11) NOT NULL,
  `meno` varchar(30) NOT NULL,
  `priezvisko` varchar(30) NOT NULL,
  `mesto` varchar(30) NOT NULL,
  `ulica` varchar(30) NOT NULL,
  `c_domu` varchar(11) NOT NULL,
  `psc` varchar(11) NOT NULL,
  `stat` varchar(30) NOT NULL,
  `telefon` varchar(30) NOT NULL,
  `datum_zajazdu` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `poznavacie`
--

CREATE TABLE `poznavacie` (
  `destinacia` varchar(30) NOT NULL,
  `subor` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `poznavacie`
--

INSERT INTO `poznavacie` (`destinacia`, `subor`) VALUES
('Vysoke_tatry', 'tatry.php'),
('Pariz', 'paris.php');

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `zajazdy_letne`
--

CREATE TABLE `zajazdy_letne` (
  `datum` varchar(30) NOT NULL,
  `destinacie` varchar(30) NOT NULL,
  `letoviska` varchar(30) NOT NULL,
  `volne_miesta` int(11) NOT NULL,
  `cena` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `zajazdy_letne`
--

INSERT INTO `zajazdy_letne` (`datum`, `destinacie`, `letoviska`, `volne_miesta`, `cena`) VALUES
('19.2.2016', 'Kreta', 'malia', 10, 200),
('19.3.2016', 'Kreta', 'chania', 20, 250),
('22.3.2016', 'Kreta', 'malia', 200, 5),
('30.6.2017', 'Severne_taliansko', 'Benatky', 200, 20),
('29.10.2017', 'Severne_taliansko', 'Bibione', 20, 1000);

-- --------------------------------------------------------

--
-- Štruktúra tabuľky pre tabuľku `zajazdy_poznavacie`
--

CREATE TABLE `zajazdy_poznavacie` (
  `datum` varchar(30) NOT NULL,
  `destinacie` varchar(30) NOT NULL,
  `letoviska` varchar(30) NOT NULL,
  `volne_miesta` int(11) NOT NULL,
  `cena` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Sťahujem dáta pre tabuľku `zajazdy_poznavacie`
--

INSERT INTO `zajazdy_poznavacie` (`datum`, `destinacie`, `letoviska`, `volne_miesta`, `cena`) VALUES
('20.1.2017', 'vysoke_tatry', 'strbske_pleso', 10, 100),
('22.1.2017', 'vysoke_tatry', 'skalnate_pleso', 50, 5),
('30.10.2017', 'pariz', 'versailes', 200, 20);

--
-- Kľúče pre exportované tabuľky
--

--
-- Indexy pre tabuľku `akcie_leto`
--
ALTER TABLE `akcie_leto`
  ADD PRIMARY KEY (`datum`,`oblast`,`mesto`);

--
-- Indexy pre tabuľku `akcie_poznavacie`
--
ALTER TABLE `akcie_poznavacie`
  ADD PRIMARY KEY (`datum`,`oblast`,`mesto`);

--
-- Indexy pre tabuľku `kontakt`
--
ALTER TABLE `kontakt`
  ADD PRIMARY KEY (`mesto`,`adresa`,`psc`,`mail`,`cislo`);

--
-- Indexy pre tabuľku `letne`
--
ALTER TABLE `letne`
  ADD PRIMARY KEY (`destinacia`);

--
-- Indexy pre tabuľku `objednavky`
--
ALTER TABLE `objednavky`
  ADD PRIMARY KEY (`ID`);

--
-- Indexy pre tabuľku `poznavacie`
--
ALTER TABLE `poznavacie`
  ADD PRIMARY KEY (`destinacia`);

--
-- Indexy pre tabuľku `zajazdy_letne`
--
ALTER TABLE `zajazdy_letne`
  ADD PRIMARY KEY (`datum`,`destinacie`,`letoviska`);

--
-- Indexy pre tabuľku `zajazdy_poznavacie`
--
ALTER TABLE `zajazdy_poznavacie`
  ADD PRIMARY KEY (`datum`,`destinacie`,`letoviska`);

--
-- AUTO_INCREMENT pre exportované tabuľky
--

--
-- AUTO_INCREMENT pre tabuľku `objednavky`
--
ALTER TABLE `objednavky`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
