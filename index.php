<?php
include 'connect_to_database.php'; 
?>

<head>
<title>CK EUBA</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />    
<link rel="stylesheet" media="all" type='text/css'   href="default.css" />
</head>
<body>

<?php include_once("analyticstracking.php") ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/sk_SK/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="outer">
  <div id="outer2">
    <div>
    <h1 align="center"> Cestovna kancelaria CK EUBA</h1><br>
    
      <h3><img src="images/euba.png" align="left" height="200" width="230" ><img src="images/dovolenka.jpg" align="center" height="200" width="528" ></h3>
    </div>
    <div id="menu" align="center">
      <ul>
        <li><a href="index.php">O nas</a></li>
        <li><a href="letne.php">Letne dovolenky</a></li>
        <li><a href="poznavacie.php">Poznavacie zajazdy</a></li>
        <li><a href="kontakt.php">Kontakt</a></li>
      </ul>
    </div>
    <div id="content">
      <div id="column1">
        <h3>Zakladne informacie</h3>
        <p>Sluzby <strong>CK EUBA</strong>  su orientovane tak, aby si mohli z ponuky vybrat rozne skupiny
zakaznikov(pedagogovia,studenti a ich rodiny). V nasej ponuke mame pobytove i poznavacie zajazdy do exotickych krajin, urcene
pre vsetky vekove kategorie. Nase sluzby chceme ponukat v takej kvalite, aby mal zakaznik pocit
kvalitne a bezstarostne stravenej dovolenky. To zabezpecime hlavne dokladnym vyberom
strediska cestovneho ruchu ako aj vhodymi ubytovacimi kapacitami, prijemnymi a ochotnymi
sprievodcami a presnymi informaciami. </p>

        <h3>Novinky, Mimoriadne informacie</h3>
        <p><font color="red"><strong >Mimoriadna informacia:</strong></font> zajazd do Pariza ,ktory sa mal konat 24.12.2016 je kvoli teroristickej hrozbe zruseny.</p>
        <p><strong>Novinka</strong>, do ponuky bude zaradene nove letovisko Bibione-Taliansko.</p>
      </div>
    </div>
      <div id="column2">
        <h3>Akciove ponuky</h3>
               <?php
include 'akcie.php'; 
                ?>
      </div>      
    <div id="footer">
      <div class="fb-like" data-href="https://www.facebook.com/ckeuba" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
      <p>Copyright &copy; 2016 CK EUBA  Design by Kratina,Hnat,Klincok </a>.</p>
    </div>
  </div>
</div>   
</body>
</html>
